﻿using Aldebaran.Proxies;
using RobotNao.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace RobotNao.Devices
{
    public class Head : Device
    {
        public Joint Yaw { get; set; }
        public Joint Pitch { get; set; }

        public Head(string a_name, MotionProxy a_motion) : base(a_name, a_motion)
        {
            Yaw = _jointsList[0];
            Pitch = _jointsList[1];
        }
    }
}
