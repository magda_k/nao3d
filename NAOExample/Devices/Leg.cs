﻿using Aldebaran.Proxies;
using RobotNao.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace RobotNao.Devices
{
    public class Leg : Device
    {
        public Joint HipYawPitch { get; set; }
        public Joint HipRoll { get; set; }
        public Joint HipPitch { get; set; }
        public Joint KneePitch { get; set; }
        public Joint AnklePitch { get; set; }
        public Joint AnkleRoll { get; set; }


        public Leg(string a_name, MotionProxy a_motion)
            : base(a_name, a_motion)
        {
            HipYawPitch = _jointsList[0];
            HipRoll = _jointsList[1];
            HipPitch = _jointsList[2];
            KneePitch = _jointsList[3];
            AnklePitch = _jointsList[4];
            AnkleRoll = _jointsList[4];

        }
    }
}
