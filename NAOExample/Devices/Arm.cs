﻿using Aldebaran.Proxies;
using RobotNao.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace RobotNao.Devices
{
    public class Arm : Device
    {
        public Joint ShoulderPitch { get; set; }
        public Joint ShoulderRoll { get; set; }
        public Joint ElbowYaw { get; set; }
        public Joint ElbowRoll { get; set; }
        public Joint WristYaw { get; set; }

        public Arm(string a_name, MotionProxy a_motion)
            : base(a_name, a_motion)
        {
            ShoulderPitch = _jointsList[0];
            ShoulderRoll = _jointsList[1];
            ElbowYaw = _jointsList[2];
            ElbowRoll = _jointsList[3];
            WristYaw = _jointsList[4];
        }
    }
}
