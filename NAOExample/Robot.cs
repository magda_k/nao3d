﻿using Aldebaran.Proxies;
using RobotNao.Devices;
using RobotNao.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace RobotNao
{
    public class Robot
    {
        private MotionProxy _motion;

        private string[] _nameList = { "Head", "LArm", "LLeg", "RLeg", "RArm" };
        public Head Head { get; set; }
        public Arm LeftArm { get; set; }
        public Leg LeftLeg { get; set; }
        public Leg RightLeg { get; set; }
        public Arm RigthArm { get; set; }


        public void Connect(string ip)
        {
            try
            {
                _motion = new MotionProxy(ip, 9559);

                Head = new Head("Head", _motion);
                LeftArm = new Arm("LArm", _motion);
                RigthArm = new Arm("RArm", _motion);
                LeftLeg = new Leg("LLeg", _motion);
                RightLeg = new Leg("RLeg", _motion);



                foreach (string name in _nameList)
                {
                    _motion.stiffnessInterpolation(name, 1.0f, 1.0f);
                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Robot.Connect exception: " + e);
            }
        }

        public void Disconnect()
        {
            if (_motion == null)
            {
                return;
            }
            try
            {
                foreach (var name in _nameList)
                {
                    _motion.stiffnessInterpolation(name, 0.0f, 0.1f);
                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Robot.Disconnect exception: " + e);
            }
        }
    }
}
