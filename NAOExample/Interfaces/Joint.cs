﻿using Aldebaran.Proxies;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace RobotNao.Interfaces
{
    public class Joint
    {
        private MotionProxy _motion;

        public string Name { get; set; }
        public float MinLimit { get; set; }
        public float MaxLimit { get; set; }

        public Joint(string name, MotionProxy _motion)
        {
            this.Name = name;
            this._motion = _motion;
        }

        public void Update(int val)
        {
            if (_motion != null)
            {
                try
                {
                    _motion.setAngles(this.Name, ScaleToRange(-val, MinLimit, MaxLimit), 0.1f);
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine(this.Name + " Update exception: " + e);
                }
            }
        }

        private static float ScaleToRange(int val, float min, float max)
        {
            float returnVal = (((val + 100.0f)/200.0f)*(max - min)) + min;
            return returnVal;
        }
    }
}
