﻿using Aldebaran.Proxies;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace RobotNao.Interfaces
{
    public class Device
    {
        public String Name { get; set; }
        protected List<Joint> _jointsList;
        protected MotionProxy _motion;

        public Device(String a_name, MotionProxy a_motion)
        {
            this.Name = a_name;
            _motion = a_motion;
            _jointsList = new List<Joint>();
     
            try
            {
                ArrayList limits = (ArrayList)_motion.getLimits(this.Name);

                foreach (string name in _motion.getBodyNames(this.Name))
                {
                    _jointsList.Add(new Joint(name,_motion));
                }

                int i = 0;
                foreach (var jointLimits in limits )
                {
                    _jointsList[i].MaxLimit = (float)(jointLimits as ArrayList)[0];
                    _jointsList[i].MinLimit = (float)(jointLimits as ArrayList)[1];
                    i++;
                } 
                
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(this.Name + " constructor exception: " + e);
            }
        }

    }
}
