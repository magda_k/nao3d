﻿/**
 * @author Aldebaran Robotics
 * Aldebaran Robotics (c) 2009 All Rights Reserved.\n
 *
 * Version : $Id$
 */
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NaoCamCSharp")]
[assembly: AssemblyDescription("Example application showing the use of NaoQiDotNet dll")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Aldebaran Robotics")]
[assembly: AssemblyProduct("NaoCamCSharp")]
[assembly: AssemblyCopyright("Copyright Aldebaran Robotics ©  2010")]
[assembly: AssemblyTrademark("Aldebaran Robotics")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8aacfd52-9ac8-43d3-b353-8831da21655b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
