﻿/**
 * @author Aldebaran Robotics
 * Aldebaran Robotics (c) 2009 All Rights Reserved.\n
 *
 * Version : $Id$
 */
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

using RobotNao;


namespace RobotNao
{
    public partial class Form1 : Form
    {
        private const int kFps = 30;

        private Robot robot;

        public Form1()
        {
            InitializeComponent();
            robot = new Robot();

        }

        private void buttonConnectToNaoCam_Click(object sender, EventArgs e)
        {
            try
            {
                robot.Connect(textBoxIP.Text);

                timerUpdate.Interval = (int)Math.Ceiling(1000.0 / kFps);

            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("Exception while connecting to NaoCam: " + ex);
            }

        }


        private void trackBar_ValueChanged(object sender, EventArgs e)
        {
            TrackBar trackBar = sender as TrackBar;
            if (trackBar != null)
            {
                if (trackBar.Name.Equals("HeadYaw"))
                {
                    robot.Head.Yaw.Update(trackBar.Value);
                }
                if (trackBar.Name.Equals("HeadPitch"))
                {
                    robot.Head.Pitch.Update(trackBar.Value);
                }
                //=======================================
                if (trackBar.Name.Equals("LShoulderPitch"))
                {
                    robot.LeftArm.ShoulderPitch.Update(trackBar.Value);
                }
                if (trackBar.Name.Equals("LSholderRoll"))
                {
                    robot.LeftArm.ShoulderRoll.Update(trackBar.Value);
                }
                if (trackBar.Name.Equals("LElbowYaw"))
                {
                    robot.LeftArm.ElbowYaw.Update(trackBar.Value);
                }
                if (trackBar.Name.Equals("LElbowRoll"))
                {
                    robot.LeftArm.ElbowRoll.Update(trackBar.Value);
                }
                if (trackBar.Name.Equals("LWristYaw"))
                {
                    robot.LeftArm.WristYaw.Update(trackBar.Value);
                }
                //=======================================
                if (trackBar.Name.Equals("LHipYawPitch"))
                {
                    robot.LeftLeg.HipYawPitch.Update(trackBar.Value);
                }
                if (trackBar.Name.Equals("LHipRoll"))
                {
                    robot.LeftLeg.HipRoll.Update(trackBar.Value);
                }
                if (trackBar.Name.Equals("LHipPitch"))
                {
                    robot.LeftLeg.HipPitch.Update(trackBar.Value);
                }
                if (trackBar.Name.Equals("LKneePitch"))
                {
                    robot.LeftLeg.KneePitch.Update(trackBar.Value);
                }
                if (trackBar.Name.Equals("LAnklePitch"))
                {
                    robot.LeftLeg.AnklePitch.Update(trackBar.Value);
                }
                if (trackBar.Name.Equals("LAnkleRoll"))
                {
                    robot.LeftLeg.AnkleRoll.Update(trackBar.Value);
                }
            }
        }

    
    }
}