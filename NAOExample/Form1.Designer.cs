﻿namespace RobotNao
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonConnectToNaoCam = new System.Windows.Forms.Button();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.trackBarYaw = new System.Windows.Forms.TrackBar();
            this.trackBarPitch = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.trackBar4 = new System.Windows.Forms.TrackBar();
            this.trackBar5 = new System.Windows.Forms.TrackBar();
            this.trackBar6 = new System.Windows.Forms.TrackBar();
            this.trackBar7 = new System.Windows.Forms.TrackBar();
            this.trackBar8 = new System.Windows.Forms.TrackBar();
            this.trackBar9 = new System.Windows.Forms.TrackBar();
            this.trackBar10 = new System.Windows.Forms.TrackBar();
            this.trackBar11 = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYaw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarPitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar11)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonConnectToNaoCam
            // 
            this.buttonConnectToNaoCam.Location = new System.Drawing.Point(227, 3);
            this.buttonConnectToNaoCam.Name = "buttonConnectToNaoCam";
            this.buttonConnectToNaoCam.Size = new System.Drawing.Size(48, 23);
            this.buttonConnectToNaoCam.TabIndex = 2;
            this.buttonConnectToNaoCam.Text = "Connect";
            this.buttonConnectToNaoCam.UseVisualStyleBackColor = true;
            this.buttonConnectToNaoCam.Click += new System.EventHandler(this.buttonConnectToNaoCam_Click);
            // 
            // textBoxIP
            // 
            this.textBoxIP.Location = new System.Drawing.Point(119, 5);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(102, 20);
            this.textBoxIP.TabIndex = 3;
            this.textBoxIP.Text = "127.0.0.1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "IP:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Head:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Left Arm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 380);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Left Leg";
            // 
            // trackBarYaw
            // 
            InitializeTrackBar(this.trackBarYaw);
            this.trackBarYaw.Location = new System.Drawing.Point(12, 87);
            this.trackBarYaw.Name = "HeadYaw";
            // 
            // trackBarPitch
            // 
            InitializeTrackBar(this.trackBarPitch);
            this.trackBarPitch.Location = new System.Drawing.Point(12, 56);
            this.trackBarPitch.Name = "HeadPitch";
            // 
            // trackBar1
            // 
            InitializeTrackBar(this.trackBar1);
            this.trackBar1.Location = new System.Drawing.Point(12, 163);
            this.trackBar1.Name = "LShoulderPitch";
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(12, 198);
            this.trackBar2.Name = "LSholderRoll";
            InitializeTrackBar(this.trackBar2);

            // 
            // trackBar3
            // 
            this.trackBar3.Location = new System.Drawing.Point(12, 249);
            this.trackBar3.Name = "LElbowYaw";
            InitializeTrackBar(this.trackBar3);

            // 
            // trackBar4
            // 
            this.trackBar4.Location = new System.Drawing.Point(12, 290);
            this.trackBar4.Name = "LElbowRoll";
            InitializeTrackBar(this.trackBar4);

            // 
            // trackBar5
            // 
            this.trackBar5.Location = new System.Drawing.Point(12, 332);
            this.trackBar5.Name = "LWristYaw";
            InitializeTrackBar(this.trackBar5);

            // 
            // trackBar6
            // 
            this.trackBar6.Location = new System.Drawing.Point(12, 396);
            this.trackBar6.Name = "LHipYawPitch";
            InitializeTrackBar(this.trackBar6);

            // 
            // trackBar7
            // 
            this.trackBar7.Location = new System.Drawing.Point(12, 447);
            this.trackBar7.Name = "LHipRoll";
            InitializeTrackBar(this.trackBar7);

            // 
            // trackBar8
            // 
            this.trackBar8.Location = new System.Drawing.Point(12, 500);
            this.trackBar8.Name = "LHipPitch";
            InitializeTrackBar(this.trackBar8);

            // 
            // trackBar9
            // 
            this.trackBar9.Location = new System.Drawing.Point(12, 536);
            this.trackBar9.Name = "LKneePitch";
            InitializeTrackBar(this.trackBar9);

            // 
            // trackBar10
            // 
            this.trackBar10.Location = new System.Drawing.Point(12, 587);
            this.trackBar10.Name = "LAnklePitch";
            InitializeTrackBar(this.trackBar10);

            // 
            // trackBar11
            // 
            this.trackBar11.Location = new System.Drawing.Point(12, 620);
            this.trackBar11.Name = "LAnkleRoll";
            InitializeTrackBar(this.trackBar11);

            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 677);
            this.Controls.Add(this.trackBar11);
            this.Controls.Add(this.trackBar10);
            this.Controls.Add(this.trackBar9);
            this.Controls.Add(this.trackBar8);
            this.Controls.Add(this.trackBar7);
            this.Controls.Add(this.trackBar6);
            this.Controls.Add(this.trackBar5);
            this.Controls.Add(this.trackBar4);
            this.Controls.Add(this.trackBar3);
            this.Controls.Add(this.trackBar2);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.trackBarPitch);
            this.Controls.Add(this.trackBarYaw);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxIP);
            this.Controls.Add(this.buttonConnectToNaoCam);
            this.Name = "Form1";
            this.Text = "NaoCamCSharpExample";
            ((System.ComponentModel.ISupportInitialize)(this.trackBarYaw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarPitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar11)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void InitializeTrackBar(System.Windows.Forms.TrackBar trackBar)
        {
            trackBar.AutoSize = false;
            trackBar.LargeChange = 20;
            trackBar.Maximum = 100;
            trackBar.Minimum = -100;
            trackBar.Size = new System.Drawing.Size(320, 25);
            trackBar.SmallChange = 2;
            trackBar.TabIndex = 5;
            trackBar.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
        }

        #endregion

        private System.Windows.Forms.Button buttonConnectToNaoCam;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timerUpdate;
        private System.Windows.Forms.TrackBar trackBarYaw;
        private System.Windows.Forms.TrackBar trackBarPitch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.TrackBar trackBar3;
        private System.Windows.Forms.TrackBar trackBar4;
        private System.Windows.Forms.TrackBar trackBar5;
        private System.Windows.Forms.TrackBar trackBar6;
        private System.Windows.Forms.TrackBar trackBar7;
        private System.Windows.Forms.TrackBar trackBar8;
        private System.Windows.Forms.TrackBar trackBar9;
        private System.Windows.Forms.TrackBar trackBar10;
        private System.Windows.Forms.TrackBar trackBar11;
    }
}

